theme-arnica
=============
Arnica - Creative Coming Soon Template


Installation
------------
The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist ommu/arnica "dev-master"
```

 or
```
 "ommu/arnica": "dev-master"
```

to the require section of your composer.json.


Preview
------------
https://theme.ommu.co/arnica-site
